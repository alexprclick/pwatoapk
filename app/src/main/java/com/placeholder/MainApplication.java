package com.placeholder;
import com.onesignal.OneSignal;

import android.app.Application;
import android.util.Log;

public class MainApplication extends Application {
    private static final String TAG = MainApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");

        // OneSignal Initialization
        OneSignal.startInit(this)
          .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
          .unsubscribeWhenNotificationsAreDisabled(true)
          .init();
    }
}
