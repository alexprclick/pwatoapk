/**
 * Automatically generated file. DO NOT MODIFY
 */
package xyz.appmaker.xmkvsg.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "xyz.appmaker.xmkvsg.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 276;
  public static final String VERSION_NAME = "1";
}
