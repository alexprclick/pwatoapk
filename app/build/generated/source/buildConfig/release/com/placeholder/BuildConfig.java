/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.placeholder;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "xyz.appmaker.xmkvsg";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 276;
  public static final String VERSION_NAME = "1";
}
